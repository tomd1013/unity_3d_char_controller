﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterDelay : MonoBehaviour
{
    private float startTime;
    [SerializeField]
    private float ttl = 5;

    private bool isCancelled = false;

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
    }

    private void Update()
    {
        if (!isCancelled && Time.time > startTime + ttl)
        {
            Destroy(gameObject);
        }
    }

    public void Cancel()
    {
        isCancelled = true;
    }

}
