﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject spawnable = null;

    [SerializeField]
    private int count = 200;

    [SerializeField]
    Transform target = null;
    [SerializeField]
    GeneralFollow follower = null;

    [SerializeField]
    float maxSpeed = 100f;
    [SerializeField]
    float sepInfl = 100f;
    [SerializeField]
    float aliInfl = 100f;
    [SerializeField]
    float cohInfl = 100f;
    [SerializeField]
    float targInfl = 100f;
    [SerializeField]
    float worldInfl = 100f;

    private List<Boids.Agent> agents = new List<Boids.Agent>();

    // Start is called before the first frame update
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Spawn();
        }
    }

    private void Spawn()
    {
        for (int i = 0; i < count; i++)
        {
            GameObject go = GameObject.Instantiate(spawnable, transform.position, transform.rotation, null);
            agents.Add(go.GetComponent<Boids.Agent>());
        }

        foreach(Boids.Agent a in agents)
        {
            a.Init(agents, target, maxSpeed, sepInfl, aliInfl, cohInfl, targInfl, worldInfl);
        }

        follower.SetTargets(agents);
    }
}
