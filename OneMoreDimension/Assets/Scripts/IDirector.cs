﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDirector
{
    Vector3 GetDirection();
}
