﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
 * Boid Steering Behaviors
 * 
 * Separation:
 *   Steer to avoid crowding local flockmates.
 * 
 * Alignment:
 *   Steer towards the average heading of local flockmates.
 * 
 * Cohesion:
 *   Steer to move towards the average position of local flockmates.
*/

namespace Boids
{
    public class Agent : MonoBehaviour
    {
        private Rigidbody rb = null;
        //[SerializeField]
        private List<Agent> neighbors = new List<Agent>();

        public static float reactionAngle = 135f;
        public static float reactionRange = 20f;

        [SerializeField]
        private float posSpawnRange = 5f;
        [SerializeField]
        private float velSpawnRange = 5f;

        [SerializeField]
        private float separationInfluence = 5f;

        [SerializeField]
        private float personalSpace = 15f;
        [SerializeField]
        private float maxSpeed = 5f;

        [SerializeField]
        private float cohesionInfluence = 100f;

        [SerializeField]
        private float alignmentInfluence = 100f;

        [SerializeField]
        private float worldBound = 50f;
        [SerializeField]
        private float worldBoundInfluence = 50f;

        [SerializeField]
        private Transform target = null;
        [SerializeField]
        private float tendToTargetInfluence = 10f;


        public Vector3 Vel
        {
            get { return rb.velocity; }
        }

        private Vector3 v1 = Vector3.zero, 
            v2 = Vector3.zero, 
            v3 = Vector3.zero,
            v4 = Vector3.zero,
            v5 = Vector3.zero;

        // Start is called before the first frame update
        void Start()
        {
            // Init direction and speed
            rb = GetComponent<Rigidbody>();
            Vector3 pos = new Vector3(Random.Range(-posSpawnRange, posSpawnRange), Random.Range(-posSpawnRange, posSpawnRange), Random.Range(-posSpawnRange, posSpawnRange));
            transform.position = pos;
            Vector3 vel = new Vector3(Random.Range(-velSpawnRange, velSpawnRange), Random.Range(-velSpawnRange, velSpawnRange), Random.Range(-velSpawnRange, velSpawnRange));
            rb.velocity = vel.normalized;
        }

        // Update is called once per frame
        void Update()
        {
            Steer();
        }

        public void FixedUpdate()
        {
            //Steer();
        }

        public void OnTriggerEnter(Collider col)
        {
            //Debug.Log("Adding neighbor...");
            Agent a = col.gameObject.GetComponent<Agent>();
            if (a != null)
            {
                neighbors.Add(a);
            }
        }

        public void OnTriggerExit(Collider col)
        {
            //Debug.Log("Removing neighbor...");
            Agent a = col.gameObject.GetComponent<Agent>();
            if (a != null && neighbors.Contains(a))
            {
                neighbors.Remove(a);
            }
        }

        public void Init(List<Agent> agents, Transform target, float maxSpeed, float sepInfl, float aliInfl, float cohInfl, float targInfl, float worldInfl)
        {
            //SetNeighbors(agents);
            this.maxSpeed = maxSpeed;
            this.target = target;
            this.separationInfluence = sepInfl;
            this.alignmentInfluence = aliInfl;
            this.cohesionInfluence = cohInfl;
            this.tendToTargetInfluence = targInfl;
            this.worldBoundInfluence = worldInfl;
        }

        public void SetNeighbors(List<Agent> agents)
        {
            neighbors = agents;
        }

        private void Steer()
        {
            v1 = Separation();
            v2 = Alignment();
            v3 = Cohesion();
            if(null != target)
            {
                v4 = TendToTarget();
            }
            v5 = WorldBound();

            /*
            Debug.DrawLine(this.transform.position, this.transform.position + v1, Color.red);
            Debug.DrawLine(this.transform.position, this.transform.position + v2, Color.cyan);
            Debug.DrawLine(this.transform.position, this.transform.position + v3, Color.green);
            Debug.DrawLine(this.transform.position, this.transform.position + v4, Color.magenta);
            Debug.DrawLine(this.transform.position, this.transform.position + v5, Color.yellow);
            */

            rb.velocity = rb.velocity + v3 + v1 + v2 + v4 + v5;
            rb.velocity = LimitVelocity(rb.velocity, maxSpeed);
        }

        private Vector3 Separation()
        {
            Vector3 dist = Vector3.zero;

            foreach (Agent n in neighbors)
            {
                if (n != this && AgentInRange(this, n))
                {

                    float d = Vector3.Distance(this.transform.position, n.transform.position);
                    if (d < personalSpace)
                    {
                        dist -= n.transform.position - this.transform.position;
                    }
                }
            }

            return dist / separationInfluence;
        }

        private Vector3 Alignment()
        {
            Vector3 vel = Vector3.zero;
            if (neighbors.Count == 0)
            {
                return vel;
            }

            foreach (Agent n in neighbors)
            {
                if(n != this && AgentInRange(this, n))
                {
                    vel += n.Vel;
                }
            }

            return (vel - rb.velocity) / alignmentInfluence;
        }

        private Vector3 Cohesion()
        {
            Vector3 com = Vector3.zero;
            if(neighbors.Count == 0)
            {
                return com;
            }

            foreach(Agent n in neighbors)
            {
                if(n != this && AgentInRange(this, n))
                {
                    com += n.transform.position;
                }
            }

            return (com/neighbors.Count)/cohesionInfluence;
        }

        private Vector3 TendToTarget()
        {
            return (target.position - this.transform.position) / tendToTargetInfluence;
        }

        private Vector3 WorldBound()
        {
            Vector3 v = Vector3.zero;
            if(this.transform.position.x > worldBound)
            {
                v.x = -(this.transform.position.x - worldBound);
            } else if(this.transform.position.x < -worldBound)
            {
                //v.x = worldBoundInfluence;
                v.x = worldBound - this.transform.position.x;
            }

            if (this.transform.position.y > worldBound)
            {
                //v.y = -worldBoundInfluence;
                v.y = -(this.transform.position.y - worldBound);
            }
            else if (this.transform.position.y < -worldBound)
            {
                //v.y = worldBoundInfluence;
                v.y = worldBound - this.transform.position.y;
            }

            if (this.transform.position.z > worldBound)
            {
                //v.z = -worldBoundInfluence;
                v.z = -(this.transform.position.z - worldBound);
            }
            else if (this.transform.position.z < -worldBound)
            {
                //v.z = worldBoundInfluence;
                v.z = worldBound - this.transform.position.z;
            }

            return v;
        }

        private static Vector3 LimitVelocity(Vector3 v, float max)
        {
            if(v.magnitude > max)
            {
                v = v.normalized * max;
            } else if(v.magnitude < 10)
            {
                v = v.normalized * 10;
            }
            return v;
        }

        private static bool AgentInRange(Agent src, Agent dst)
        {
            if(Vector3.Distance(dst.transform.position, src.transform.position) > reactionRange)
            {
                return false;
            }

            if(Vector3.Angle(src.transform.forward, (dst.transform.position-src.transform.position)) > reactionAngle)
            {
                return false;
            }

            return true;
        }
    }
}

