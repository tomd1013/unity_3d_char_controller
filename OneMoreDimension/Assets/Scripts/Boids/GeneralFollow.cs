﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralFollow : MonoBehaviour
{
    private List<Boids.Agent> objs = new List<Boids.Agent>();
    private Vector3 focus = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetTargets(List<Boids.Agent> objs)
    {
        this.objs = objs;
    }

    // Update is called once per frame
    void Update()
    {
        focus = Vector3.zero;
        if(objs.Count > 0)
        {
            foreach (Boids.Agent go in objs)
            {
                focus += go.transform.position;
            }
            focus /= objs.Count;
        }

        this.transform.LookAt(focus);
    }
}
