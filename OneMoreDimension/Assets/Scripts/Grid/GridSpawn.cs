﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSpawn : MonoBehaviour
{
    public int gridSize = 5;
    public List<Cell> cells = new List<Cell>();
    public GameObject cellPrefab;

    // Start is called before the first frame update
    void Start()
    {
        string classStr = "Cell";
        Type clazz = Type.GetType(classStr);
        if(clazz == typeof(Cell))
        {
            Debug.Log("Worked?");
        }

        for (int i = 0; i<gridSize; i++)
        {
            for (int j = 0; j<gridSize; j++)
            {
                Vector3 pos = new Vector3(transform.position.x + i ,0, transform.position.z + j);
                GameObject cGO = GameObject.Instantiate(cellPrefab, pos, transform.rotation, transform);
                cells.Add(cGO.GetComponent<Cell>());
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
