﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO(Tom): Instead of splitting logic with an enum, separate into two camera classes
public class CameraFollow : MonoBehaviour, IDirector
{
    public bool isControlled = false;

    private enum CamType
    {
        DARK_SOULS,
        POINT_CLICK
    }
    [SerializeField]
    private CamType camType = CamType.DARK_SOULS;

    [SerializeField]
    private GameObject target = null;

    [SerializeField]
    private float distance = 20;

    [SerializeField]
    private float sensitivity = 10;

    private Vector3 moveVec = Vector3.zero;
    private Vector3 camForward = Vector3.zero;
    private Vector3 camRight = Vector3.zero;
    private bool isReached = true; // Used for point-and-click mode.

    // Update is called once per frame
    void Update()
    {
        OrbitCamera();
    }

    private void OrbitCamera()
    {
        switch (camType)
        {
            case CamType.DARK_SOULS:
                OrbitCameraMouse();
                break;
            case CamType.POINT_CLICK:
                OrbitCameraWASD();
                break;
            default:
                OrbitCameraMouse();
                break;
        }
    }

    private void OrbitCameraMouse()
    {
        if (isControlled)
        {
            float rotateHorizontal = Input.GetAxis("Mouse X");
            float rotateVertical = Input.GetAxis("Mouse Y");

            transform.RotateAround(target.transform.position, transform.up, rotateHorizontal * sensitivity);
            transform.RotateAround(target.transform.position, transform.right, rotateVertical * sensitivity);
        }
        transform.LookAt(target.transform);

        transform.position = (transform.position - target.transform.position).normalized * distance + target.transform.position;
    }

    private void OrbitCameraWASD()
    {
        if (isControlled)
        {
            float rotateHorizontal = Input.GetAxis("Horizontal");
            float rotateVertical = Input.GetAxis("Vertical");

            transform.RotateAround(target.transform.position, transform.up, rotateHorizontal * sensitivity);
            transform.RotateAround(target.transform.position, transform.right, rotateVertical * sensitivity);
        }
        transform.LookAt(target.transform);

        transform.position = (transform.position - target.transform.position).normalized * distance + target.transform.position;
    }

    public Vector3 GetDirection()
    {
        if (!isControlled)
        {
            return Vector3.zero;
        }

        switch (camType)
        {
            case CamType.DARK_SOULS:
                return GetDirectionWASD();
            case CamType.POINT_CLICK:
                return GetDirectionPointClick();
            default:
                return GetDirectionWASD();
        }
    }

    private Vector3 GetDirectionWASD()
    {
        float forward = Input.GetAxisRaw("Vertical");
        float right = Input.GetAxisRaw("Horizontal");
        if (forward != 0 || right != 0)
        {
            camForward = transform.forward;
            camRight = transform.right;
            camForward.y = 0;
            camRight.y = 0;
            moveVec = camForward * forward + camRight * right;

            return moveVec.normalized;
        }
        else
        {
            return Vector3.zero;
        }
    }

    private Vector3 GetDirectionPointClick()
    {
        if (Input.GetMouseButtonDown(1))
        {
            isReached = false;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit))
            {
                moveVec = hit.point;
                moveVec.y = 0;
            }
        }

        if (isReached)
        {
            moveVec = Vector3.zero;
            return moveVec;
        } else
        {
            if((moveVec - target.transform.position).sqrMagnitude <= 9)
            {
                isReached = true;
                return Vector3.zero;
            }
        }

        Vector3 pos = target.transform.position;
        pos.y = 0;
        return (moveVec - pos).normalized;
    }
}
