﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplitScreenHandler : MonoBehaviour
{
    [SerializeField]
    List<Camera> cams = null;

    // Start is called before the first frame update
    void Start()
    {
        SetCameras();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetCameras()
    {
        if (cams.Count == 1)
        {
            cams[0].rect = new Rect(0, 0, 1, 1);
        }
        else
        {
            int rows = ((cams.Count + 1) / 2);
            float xScale = 0.5f;
            float yScale = 1.0f / rows;
            float yOff = 1 - yScale;
            for (int i = 0; i < cams.Count; i++)
            {
                float xOff = 0;
                if (i % 2 == 1)
                {
                    xOff = 0.5f;
                }
                cams[i].rect = new Rect(xOff, yOff, xScale, yScale);
                if (i % 2 == 1)
                {
                    yOff -= yScale;
                }
            }
        }
    }
}
