﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLogic : MonoBehaviour
{
    private GameObject parent;
    private DestroyAfterDelay destroyer;
    private Rigidbody rb;
    private MeshRenderer renderer;
    private BoxCollider collider;
    // Start is called before the first frame update
    void Start()
    {
        destroyer = GetComponent<DestroyAfterDelay>();
        rb = GetComponent<Rigidbody>();
        renderer = GetComponent<MeshRenderer>();
        renderer.material.color = Color.green;
        collider = GetComponent<BoxCollider>();
    }

    public void SetParent(GameObject go)
    {
        parent = go;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer != gameObject.layer && collision.gameObject != parent)
        {
            destroyer.Cancel();
            rb.velocity = Vector3.zero;
            rb.isKinematic = true;
            collider.enabled = false;
            gameObject.transform.SetParent(collision.gameObject.transform);
            gameObject.layer = collision.gameObject.layer;
            
            renderer.material.color = Color.red;
        }
    }
}
