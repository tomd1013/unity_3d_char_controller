﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public bool isControlled = false;

    private Rigidbody rb = null;

    [SerializeField]
    private GameObject directorGO = null;
    private IDirector director = null;
    [SerializeField]
    private float moveSpeed = 5;

    private MeshRenderer renderer = null;
    [SerializeField]
    private List<Color> colorList = null;
    private int curColor = 0;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<MeshRenderer>();
        rb = GetComponent<Rigidbody>();
        director = directorGO.GetComponent<IDirector>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = director.GetDirection();
        if(dir != Vector3.zero)
        {
            rb.MoveRotation(Quaternion.LookRotation(dir));
            rb.MovePosition(transform.position + transform.forward * moveSpeed * Time.deltaTime);
        }

        if (isControlled)
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                NextColor();
            } else if (Input.GetKeyDown(KeyCode.J))
            {
                PrevColor();
            }
        }
    }

    public void NextColor()
    {
        curColor++;
        if(curColor >= colorList.Count)
        {
            curColor = 0;
        }
        ChangeColor(colorList[curColor]);
    }

    public void PrevColor()
    {
        curColor--;
        if (curColor < 0)
        {
            curColor = colorList.Count - 1;
        }
        ChangeColor(colorList[curColor]);
    }

    public void ChangeColor(Color c)
    {
        renderer.material.color = c;
    }
}
