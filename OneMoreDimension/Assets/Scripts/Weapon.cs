﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public bool isControlled = false;

    [SerializeField]
    private int dmg;
    [SerializeField]
    private int spd;

    private GameObject parent;

    [SerializeField]
    private GameObject bulletSpawnPos = null;
    [SerializeField]
    private GameObject bulletPrefab = null;

    private bool isFireAvailable = true;
    private bool isFire = false;
    // Start is called before the first frame update
    void Start()
    {
        parent = gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (isControlled)
        {

            float input = Input.GetAxisRaw("Fire1");
            if (isFireAvailable && !isFire && input != 0)
            {
                isFire = true;
                isFireAvailable = false;
            } else if (!isFireAvailable && input == 0)
            {
                isFireAvailable = true;
            }

            if (isFire)
            {
                GameObject bulletGO = GameObject.Instantiate(bulletPrefab, bulletSpawnPos.transform.position, bulletSpawnPos.transform.rotation, null);
                bulletGO.GetComponent<BulletLogic>().SetParent(parent);
                bulletGO.GetComponent<Rigidbody>().velocity = bulletGO.transform.forward * spd;
                isFire = false;
            }
        }
    }
}
